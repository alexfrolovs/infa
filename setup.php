<?php
require_once 'connection.php';
 
$link = mysqli_connect($host, $user, $password, $database) 
    or die ("Ошибка подключения к базе данных" . mysqli_error());

echo "Вы подключились!<br>";

$sql = "CREATE TABLE club (
  club varchar(10) NOT NULL DEFAULT ",
  experts varchar(100) DEFAULT NULL,
  members varchar(100) DEFAULT NULL,
  PRIMARY KEY (club)
)";
if (mysqli_query($link, $sql)) {
  echo "Лаби";
} else {
  echo "Сликти" . mysqli_error($link);
}

$sql = "INSERT INTO club VALUES ('BREED','list1','list2'),('Mumu','list3','list4'),('SC','list5','list6');";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные<br>";
} else {
  echo "Заново <br>" . mysqli_error($link);
}

$sql = "CREATE TABLE dog (
  dog_name varchar(30) NOT NULL DEFAULT ",
  breed varchar(30) DEFAULT NULL,
  class varchar(30) DEFAULT NULL,
  age int(11) DEFAULT NULL,
  pedigree int(11) DEFAULT NULL,
  parents varchar(60) DEFAULT NULL,
  date_vaccin date DEFAULT NULL,
  PRIMARY KEY (`dog_name`)
)";
if (mysqli_query($link, $sql)) {
  echo "Лаби";
} else {
  echo "Сликти" . mysqli_error($link);
}

$sql = "INSERT INTO dog VALUES ('Becky','english dog','breed','2','420','Casper & Lilu','2016-10-12'),('Lucky','pug','show','3','806','Jessy & Dilan','2016-11-03'),('Melissa','york','pet','1','152','Tarany & Caleb','2016-11-14'),('Persy','poodle','breed','5','711','Blake & Rox','2016-11-23'),('Ri','chichuachua','pet','2','637','Volter & Scott','2017-01-09'),('Simon','sheep-dog','breed','7','583','Mona & Riky','2016-10-06');";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные<br>";
} else {
  echo "Заново <br>" . mysqli_error($link);
}

$sql = "CREATE TABLE dogs_show (
  id_show int(11) NOT NULL DEFAULT ",
  type varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_show`)
)";
if (mysqli_query($link, $sql)) {
  echo "Лаби";
} else {
  echo "Сликти" . mysqli_error($link);
}

$sql = "INSERT INTO dogs_shows VALUES ('1','mono'),('2','poly'),('3','poly'),('4','mono'),('5','poly');";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные<br>";
} else {
  echo "Заново <br>" . mysqli_error($link);
}

$sql = "CREATE TABLE ring (
  id_ring int(11) NOT NULL DEFAULT 0,
  name_exp varchar(30) DEFAULT NULL,
  dogs varchar(100) DEFAULT NULL,
  hours varchar(30) DEFAULT NULL,
  PRIMARY KEY (id_ring)
)";
if (mysqli_query($link, $sql)) {
  echo "Лаби";
} else {
  echo "Сликти" . mysqli_error($link);
}

$sql = "INSERT INTO ring VALUES ('1','Sergeev','dogs_r1','10am - 3pm'),('2','Romanov','dogs_r2','9.30am - 11.30am'),('3','Antonov','dogs_r3','9am - 12pm'),('4','Solovyov','dogs_r4','10am - 1pm'),('5','Romanov','dogs_r5','11am - 2.30pm');";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные<br>";
} else {
  echo "Заново <br>" . mysqli_error($link);
}

$sql = "CREATE TABLE action (
  dog_name varchar(30) NOT NULL DEFAULT 
  id_show int(11) NOT NULL DEFAULT 0,
  breed varchar(30) DEFAULT NULL,
  id_ring int(11) NOT NULL DEFAULT 0,
  name_exp varchar(30) DEFAULT NULL,
  points1 int(11) DEFAULT NULL,
  points2 int(11) DEFAULT NULL,
  points3 int(11) DEFAULT NULL,
  PRIMARY KEY (dog_name,id_show,id_ring),
  KEY id_show (id_show),
  KEY id_ring (id_ring),
  CONSTRAINT action_ibfk_3 FOREIGN KEY (id_ring) REFERENCES ring (id_ring),
  CONSTRAINT action_ibfk_1 FOREIGN KEY (dog_name) REFERENCES dog (dog_name),
  CONSTRAINT action_ibfk_2 FOREIGN KEY (id_show) REFERENCES dogs_show (id_show)
)";
if (mysqli_query($link, $sql)) {
  echo "Лаби";
} else {
  echo "Сликти" . mysqli_error($link);
}

$sql = "INSERT INTO action VALUES ('Melissa','1','york','4','Solovyova','100','96','88'),('Melissa','3','york','3','Antonova','85','97','92'),('Persy','2','poodle','5','Romanov','92','99','85'),('Ri','3','chichuachua','3','Antonova','74','85','76'),('Simon','4','sheep-dog','1','Sergeev','68','74','81');";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные<br>";
} else {
  echo "Заново <br>" . mysqli_error($link);
}

$sql = "CREATE TABLE expert (
  name_exp varchar(30) NOT NULL DEFAULT ",
  club varchar(30) DEFAULT NULL,
  PRIMARY KEY (name_exp),
  KEY club (club),
  CONSTRAINT expert_ibfk_1 FOREIGN KEY (club) REFERENCES club (club)
)";
if (mysqli_query($link, $sql)) {
  echo "Лаби";
} else {
  echo "Сликти" . mysqli_error($link);
}

$sql = "INSERT INTO expert VALUES ('Romanov','BREED'),('Sergeev','BREED'),('Antonova','Mumu'),('Solovyov','SC');";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные<br>";
} else {
  echo "Заново <br>" . mysqli_error($link);
}

$sql = "CREATE TABLE med_exam (
  pay int(11) DEFAULT NULL,
  dog_name varchar(30) NOT NULL DEFAULT ",
  breed varchar(30) DEFAULT NULL,
  doctor_name varchar(30) DEFAULT NULL,
  date_med_exam date DEFAULT NULL,
  PRIMARY KEY (dog_name),
  CONSTRAINT med_exam_ibfk_1 FOREIGN KEY (dog_name) REFERENCES dog (dog_name)
)";
if (mysqli_query($link, $sql)) {
  echo "Лаби";
} else {
  echo "Сликти" . mysqli_error($link);
}

$sql = "INSERT INTO med_exam VALUES ('1','Lucky','pug','Malakhov','2017-01-10'),('1','Melissa','york','Semakin','2016-11-29'),('1','Persy','poodle','Malakhov','2017-01-09'),('0','Ri','chichuachua','Volkova','2016-12-26'),('0','Simon','sheep-dog','Volkova','2017-01-25');";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные<br>";
} else {
  echo "Заново <br>" . mysqli_error($link);
}

$sql = "CREATE TABLE owner (
  owner_name varchar(30) NOT NULL DEFAULT ",
  dog_name varchar(30) DEFAULT NULL,
  breed varchar(30) DEFAULT NULL,
  club varchar(20) DEFAULT NULL,
  passport int(11) DEFAULT NULL,
  PRIMARY KEY (owner_name),
  KEY dog_name (dog_name),
  KEY club (club),
  CONSTRAINT owner_ibfk_2 FOREIGN KEY (club) REFERENCES club(club),
  CONSTRAINT owner_ibfk_1 FOREIGN KEY (dog_name) REFERENCES dog (dog_name)
)";
if (mysqli_query($link, $sql)) {
  echo "Лаби";
} else {
  echo "Сликти" . mysqli_error($link);
}

$sql = "INSERT INTO owner VALUES ('Arina','Ri','pug','Mumu','223501'),('Ksu','Persy','poodle','SC','301218'),('Michael','Lucky','pug','BREED','290519'),('Paul','Simon','terrier','BREED','90013'),('Veronika','Melissa','york','SC','150998');";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные<br>";
} else {
  echo "Заново <br>" . mysqli_error($link);
}

$sql = "CREATE TABLE pay (
  pay int(11) DEFAULT NULL,
  dog_name varchar(30) NOT NULL DEFAULT ",
  breed varchar(30) DEFAULT NULL,
  sum int(11) DEFAULT NULL,
  PRIMARY KEY (dog_name),
  CONSTRAINT pay_ibfk_1 FOREIGN KEY (dog_name) REFERENCES dog (dog_name)
)";
if (mysqli_query($link, $sql)) {
  echo "Лаби";
} else {
  echo "Сликти" . mysqli_error($link);
}

$sql = "INSERT INTO pay VALUES ('0','Lucky','pug','1500'),('1','Melissa','york','1100');";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные<br>";
} else {
  echo "Заново <br>" . mysqli_error($link);
}

$sql = "CREATE TABLE sponsor (
  name_sponsor varchar(30) NOT NULL DEFAULT ",
  id_show int(11) DEFAULT NULL,
  donation int(11) DEFAULT NULL,
  PRIMARY KEY (name_sponsor),
  KEY id_show (id_show),
  CONSTRAINT sponsor_ibfk_1 FOREIGN KEY (id_show) REFERENCES dogs_show (id_show)
)";
if (mysqli_query($link, $sql)) {
  echo "Лаби";
} else {
  echo "Сликти" . mysqli_error($link);
}

$sql = "INSERT INTO sponsor VALUES ('Alexey','1','12000'),('Michael','2','32000');";
if (mysqli_query($link, $sql)) {
  echo "Добавили данные<br>";
} else {
  echo "Заново <br>" . mysqli_error($link);
}

mysqli_close($link);
?>